import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import io.appium.java_client.MobileDriver as GlobalVariable
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

Mobile.startApplication(GlobalVariable.APP_SHOPNOW, false)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

String password = 'P@ssword123'

String email = 'new@delivery.net'

Mobile.tap(findTestObject('ShopNow/input_Email_Address'), 5)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('ShopNow/input_Email_Address'), email, 0)

Mobile.tap(findTestObject('ShopNow/tab_Shopnow'), 0)

Mobile.tap(findTestObject('ShopNow/input_Password'), 5)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('ShopNow/input_Password'), password, 0)

Mobile.tap(findTestObject('ShopNow/btn_Login'), 5)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()