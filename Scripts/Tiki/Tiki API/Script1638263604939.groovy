import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'login tiki API and get token'
ResponseObject reponse = WS.sendRequest(findTestObject('Testing API/TIKI Login', [('email') : 'ngovanngoc2014@gmail.com'
            , ('url') : 'https://tiki.vn']))

JsonSlurper parser = new JsonSlurper()

def afterparsing = parser.parseText(reponse.getResponseBodyContent())

String accToken = afterparsing.access_token

println('acccc = ' + afterparsing.access_token)

'update info account to token'
ResponseObject reponseToken = WS.sendRequest(findTestObject('Testing API/TIKI Edit Info', [('url') : 'https://tiki.vn', ('token') : accToken]))

def afterparsingToken = parser.parseText(reponseToken.getResponseBodyContent())

println('idddddd = ' + afterparsingToken.id)

println('name  = ' + afterparsingToken.name)

'get address to account'
ResponseObject reponseGetAddress = WS.sendRequest(findTestObject('Testing API/TIKI Get Address', [('token') : accToken]))

def afterparsingAddress = parser.parseText(reponseGetAddress.getResponseBodyContent())

println('List address  = ' + afterparsingAddress)

'Test case Create new Address and verify Address create new'
ResponseObject reponseCreateNewAddress = WS.sendRequest(findTestObject('Testing API/TIKI Create New Address', [('token') : accToken]))

def afterparsingcreateAddress = parser.parseText(reponseCreateNewAddress.getResponseBodyContent())

String id = afterparsingcreateAddress.id

println('new address  = ' + afterparsingcreateAddress)

println('id  = ' + id)

'delete address'
ResponseObject reponseDeleteAddress = WS.sendRequest(findTestObject('Testing API/TIKI DeleteAddress', [('addressID') : id, ('token') : accToken]))

def afterparsingDeleteAddress = parser.parseText(reponseDeleteAddress.getResponseBodyContent())

println('success = ' + afterparsingDeleteAddress.success)





