import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Start applicaion'
Mobile.startApplication(GlobalVariable.AND_APP_APIDEMO, true)

'Get devices'
checkDevices = Mobile.getDeviceManufacturer()

println('bbbbbbbbbbbbbbbbbbbbbb ' + checkDevices)

'Click to Accessibility'
Mobile.tap(findTestObject('Object Demo App API/btn_widget_textView _Accessibility'), 0)

'Click to Accessibility Node Querying '
Mobile.tap(findTestObject('Object Demo App API/btn_widget_textView _Accessibility Node Querying'), 0)

Mobile.tap(findTestObject('Object Demo App API/btn_Take out trash'), 0)

String checkText = Mobile.getElementHeight(findTestObject('Object Demo App API/list_checkBox'), 0)

if (Mobile.getAttribute(findTestObject('Object Demo App API/list_checkBox'), 'height', 10).equals(checkText)) {
    println('aaaaaaaaaaaaa  ' + checkText)
}

'Click check box Do Laundry'
if (Mobile.getAttribute(findTestObject('Object Demo App API/chk_checkBox_doLaundry'), 'checked', 10) != 'true') {
    Mobile.tap(findTestObject('Object Demo App API/chk_checkBox_doLaundry'), 0)
}

'Click check box Do Taxes'
if (Mobile.getAttribute(findTestObject('Object Demo App API/chk _checkBox_doTaxes'), 'checked', 10) != 'true') {
    Mobile.tap(findTestObject('Object Demo App API/chk _checkBox_doTaxes'), 0)
}

'Click check box take out trash'
if (Mobile.getAttribute(findTestObject('Object Demo App API/chk _checkBox_takeOutTrash'), 'checked', 10) != 'true') {
    Mobile.tap(findTestObject('Object Demo App API/chk _checkBox_takeOutTrash'), 0)
}

Mobile.pressBack()

'click to accessibility servecs'
Mobile.tap(findTestObject('Object Demo App API/btn_widget_textView _Accessibility Service'), 0)

Mobile.pressBack()

Mobile.closeApplication()