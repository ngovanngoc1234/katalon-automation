import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open app'
Mobile.startApplication(GlobalVariable.AND_APP_APIDEMO, true)

'Click to bnt OS'
Mobile.tap(findTestObject('Send Message API/btn_OS'), 0)

'Click to bnt SMS message'
Mobile.tap(findTestObject('Send Message API/btn_SMS Messaging'), 0)

'Send key recipient'
Mobile.setText(findTestObject('Send Message API/input_text_Recipient'), '123123132', 0)

'Click message body'
Mobile.tap(findTestObject('Send Message API/input_messageBody'), 0)

'Send key to message body'
Mobile.setText(findTestObject('Send Message API/input_messageBody'), 'test appium', 0)

Mobile.hideKeyboard()

'Click btn father'
Mobile.tap(findTestObject('Send Message API/btn_fatherSEND'), 0)

'Click to SEND'
Mobile.tap(findTestObject('Send Message API/btn_ SEND'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()