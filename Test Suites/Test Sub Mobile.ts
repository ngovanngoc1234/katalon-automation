<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Sub Mobile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e3ac2340-8f69-4f56-97f6-4319cf91daeb</testSuiteGuid>
   <testCaseLink>
      <guid>9e8d7daa-6a25-4658-80e0-839d6e728864</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo App API/Demo click checkbox</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>417d2dc9-89e6-4310-a695-d9a54d102738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Demo App API/Send Message API</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3b9856e-b13c-4a38-b3d1-197207a924a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Calculator/Calculator mobile</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
