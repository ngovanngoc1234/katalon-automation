<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_views_DragAndDrop</name>
   <tag></tag>
   <elementGuidId>ec44388e-56bd-4079-a482-e02495b42b0f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>android:id/text1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Drag and Drop</value>
   </webElementProperties>
   <locator>/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.ListView/android.widget.TextView[8][count(. | //*[@resource-id = 'android:id/text1' and (@text = 'Drag and Drop' or . = 'Drag and Drop')]) = count(//*[@resource-id = 'android:id/text1' and (@text = 'Drag and Drop' or . = 'Drag and Drop')])]</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
